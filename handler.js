'use strict';

const https = require('https');
const vision = require('@google-cloud/vision');

const LINE_TOKEN = process.env['LINE_TOKEN'];

module.exports.helloWorld = (event, context, callback) => {

    let get_image = (message_id, callback) => {
        const headers = {
            host: 'api.line.me',
            path: '/v2/bot/message/' + message_id + '/content',
            headers: {
                "Content-type": "application/json; charset=UTF-8",
                "Authorization": " Bearer " + "{ " + LINE_TOKEN + " }"
            },
            method: 'GET'
        };

        let req = https.request(headers, (res) => {
            let data = [];
            res.on('data', (chunk) => {
                data.push(new Buffer(chunk));
            }).on('error', (err) => {
                console.log(err);
            }).on('end', () => {
                callback(data);
            });
        });
        req.end();
    };

    let translate_from_image = (image_file) => {
        const client = new vision.ImageAnnotatorClient();
        client
            .textDetection(Buffer.concat(image_file))
            .then(results => {
                console.log(JSON.stringify(results));
                results.forEach(result => {
                    let detections = result.textAnnotations;
                    detections.forEach(text => console.log("text: " + JSON.stringify(text)));
                });
            })
            .catch(err => {
                console.error('ERROR:', err);
            });
    };

    const response = {
        statusCode: 200,
        headers: {
            'Access-Control-Allow-Origin': '*', // Required for CORS support to work
        },
        body: JSON.stringify({
            message: 'Go Serverless v1.0! Your function executed successfully!',
        }),
    };

    // Lineから画像を受け取り、OCRする
    let body = JSON.parse(event.body);
    let message = body.events[0].message;
    get_image(message.id, translate_from_image);

    // TODO: OCR結果を元に、メニューの単語を生成、画像検索する

    callback(null, response);
};