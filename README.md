# デプロイ方法

## Prerequisite
- AWSアカウント + AWS CLI

- Serverless Framework
  - https://serverless.com/

- Node.js v8

## AWS設定
`serverless deploy`コマンドで行ける…はず。(未確認)

API GateWayとLambdaがデプロイされることを確認。
API GateWayのURLを確認。

## Line Botアカウント
LINE@とLine BOTの2つの設定が必要？

https://admin-official.line.me/
https://developers.line.me/ja/

Webhook送信先に、API GateWayのURLを設定。
LINE TOKENを確認。

## LINE TOKEN設定
Lambdaの環境変数`LINE_TOKEN`に、LINE TOKENを設定。

## Google
https://cloud.google.com/genomics/downloading-credentials-for-api-access?hl=ja

この辺を見ながら、credential.jsonを作成。
Google Cloud Vision APIを有効化しておくこと。
作成したJSONを`conf`フォルダ配下に格納する

## GOOGLE_APPLICATION_CREDENTIALS設定
Lambdaの環境変数`GOOGLE_APPLICATION_CREDENTIALS設定`に、jsonへのパスを設定。

## serverless 実行
- npm update --save
- serverless deploy

